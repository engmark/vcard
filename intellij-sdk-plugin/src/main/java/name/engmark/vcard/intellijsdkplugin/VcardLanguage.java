package name.engmark.vcard.intellijsdkplugin;

import com.intellij.lang.Language;

public class VcardLanguage extends Language {
  public static final VcardLanguage INSTANCE = new VcardLanguage();

  private VcardLanguage() {
    super("vCard");
  }
}
