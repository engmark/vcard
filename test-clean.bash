#!/usr/bin/env bash

set -o errexit -o noclobber -o nounset -o pipefail
shopt -s failglob inherit_errexit

files_to_clean="$(git clean -d --dry-run -x | tee /dev/stderr)"

[[ -z "${files_to_clean}" ]]
