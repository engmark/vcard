"""Setup configuration"""

import test
from glob import glob
from os import path

from setuptools import setup

from vcard import __author__, __email__, __license__, __maintainer__
from vcard import __package__ as package
from vcard import __url__

this_directory = path.abspath(path.dirname(__file__))
with open(path.join(this_directory, "README.md"), encoding="utf-8") as file_handle:
    long_description = file_handle.read()

with open(path.join(this_directory, "vcard/version.txt"), encoding="utf-8") as file_handle:
    version = file_handle.read().rstrip("\n")

setup(
    name=package,
    version=version,
    description="vCard validator, class and utility functions",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url=__url__,
    keywords="vCard vCards RFC 2426 RFC2426 validator",
    packages=[package],
    include_package_data=True,
    data_files=[("bash-completion", glob("bash-completion/*"))],
    setup_requires=[],
    install_requires=["python-dateutil~=2.9.0"],
    entry_points={"console_scripts": [f"{package}={package}.{package}:main"]},
    classifiers=[
        "Development Status :: 4 - Beta",
        "Environment :: Console",
        "Intended Audience :: End Users/Desktop",
        "License :: OSI Approved :: GNU Affero General Public License v3",
        "Natural Language :: English",
        "Operating System :: OS Independent",
        "Programming Language :: Python :: 3.8",
        "Programming Language :: Python :: 3.9",
        "Programming Language :: Python :: 3.10",
        "Programming Language :: Python :: 3.11",
        "Programming Language :: Python :: 3.12",
        "Programming Language :: Python :: 3.13",
        "Topic :: Text Processing",
        "Topic :: Utilities",
    ],
    test_suite=test.__package__,
    author=__author__,
    author_email=__email__,
    maintainer=__maintainer__,
    maintainer_email=__email__,
    download_url="https://pypi.org/project/vcard/",
    platforms=["POSIX", "Windows"],
    license=__license__,
    obsoletes=["vcard_module"],
)
