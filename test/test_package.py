import codecs
from doctest import testmod
from os.path import dirname, join
from re import escape
from typing import Optional, Tuple, TypedDict
from unittest import TestCase
from warnings import catch_warnings

from vcard import (
    vcard,
    vcard_definitions,
    vcard_errors,
    vcard_utils,
    vcard_validator,
    vcard_validators,
)

TEST_DIRECTORY = dirname(__file__)


def _get_vcard_file(path: Optional[str]) -> str:
    """Get the vCard contents locally or remotely.

    @param path: File relative to current directory or a URL
    @return: Text in the given file"""
    if path == "" or path is None:
        return ""

    filename = join(TEST_DIRECTORY, path)

    with codecs.open(filename, "r", "utf-8") as file_pointer:
        contents = file_pointer.read()

    return contents


# vCards with errors
VCARD_TEST_CASE_TYPE = TypedDict(
    "VCARD_TEST_CASE_TYPE", {"message": str, "vcards": Tuple[Optional[str], ...]}
)

VCARDS_CONTINUATION_AT_START: VCARD_TEST_CASE_TYPE = {
    "message": vcard_errors.NOTE_CONTINUATION_AT_START,
    "vcards": ("continuation_at_start.vcf",),
}
VCARDS_DOT_AT_LINE_START: VCARD_TEST_CASE_TYPE = {
    "message": vcard_errors.NOTE_DOT_AT_LINE_START,
    "vcards": ("dot_at_line_start.vcf",),
}
VCARDS_EMPTY_VCARD: VCARD_TEST_CASE_TYPE = {
    "message": vcard_errors.NOTE_EMPTY_VCARD,
    "vcards": ("", None),
}
VCARDS_INVALID_DATE: VCARD_TEST_CASE_TYPE = {
    "message": vcard_errors.NOTE_INVALID_DATE,
    "vcards": tuple(),
}
VCARDS_INVALID_LANGUAGE_VALUE: VCARD_TEST_CASE_TYPE = {
    "message": vcard_errors.NOTE_INVALID_LANGUAGE_VALUE,
    "vcards": ("invalid_language_value.vcf",),
}
VCARDS_INVALID_LINE_SEPARATOR: VCARD_TEST_CASE_TYPE = {
    "message": vcard_errors.NOTE_INVALID_LINE_SEPARATOR,
    "vcards": ("line_ending_mac.vcf", "line_ending_unix.vcf", "line_ending_mixed.vcf"),
}
VCARDS_INVALID_PARAM_NAME: VCARD_TEST_CASE_TYPE = {
    "message": vcard_errors.NOTE_INVALID_PARAMETER_NAME,
    "vcards": ("invalid_param_name.vcf",),
}
VCARDS_INVALID_PARAM_VALUE: VCARD_TEST_CASE_TYPE = {
    "message": vcard_errors.NOTE_INVALID_PARAMETER_VALUE,
    "vcards": ("invalid_param_value.vcf",),
}
VCARDS_INVALID_PROPERTY_NAME: VCARD_TEST_CASE_TYPE = {
    "message": vcard_errors.NOTE_INVALID_PROPERTY_NAME,
    "vcards": ("invalid_property_foo.vcf",),
}
VCARDS_INVALID_SUB_VALUE: VCARD_TEST_CASE_TYPE = {
    "message": vcard_errors.NOTE_INVALID_SUB_VALUE,
    "vcards": tuple(),
}
VCARDS_INVALID_SUB_VALUE_COUNT: VCARD_TEST_CASE_TYPE = {
    "message": vcard_errors.NOTE_INVALID_SUB_VALUE_COUNT,
    "vcards": tuple(),
}
VCARDS_INVALID_TEXT_VALUE: VCARD_TEST_CASE_TYPE = {
    "message": vcard_errors.NOTE_INVALID_TEXT_VALUE,
    "vcards": tuple(),
}
VCARDS_INVALID_TIME: VCARD_TEST_CASE_TYPE = {
    "message": vcard_errors.NOTE_INVALID_TIME,
    "vcards": tuple(),
}
VCARDS_INVALID_TIME_ZONE: VCARD_TEST_CASE_TYPE = {
    "message": vcard_errors.NOTE_INVALID_TIME_ZONE,
    "vcards": tuple(),
}
VCARDS_INVALID_URI: VCARD_TEST_CASE_TYPE = {
    "message": vcard_errors.NOTE_INVALID_URI,
    "vcards": tuple(),
}
VCARDS_INVALID_VALUE: VCARD_TEST_CASE_TYPE = {
    "message": vcard_errors.NOTE_INVALID_VALUE,
    "vcards": ("invalid_begin.vcf",),
}
VCARDS_INVALID_VALUE_COUNT: VCARD_TEST_CASE_TYPE = {
    "message": vcard_errors.NOTE_INVALID_VALUE_COUNT,
    "vcards": ("invalid_value_count_wp.vcf",),
}
VCARDS_INVALID_X_NAME: VCARD_TEST_CASE_TYPE = {
    "message": vcard_errors.NOTE_INVALID_X_NAME,
    "vcards": tuple(),
}
VCARDS_MISMATCH_GROUP: VCARD_TEST_CASE_TYPE = {
    "message": vcard_errors.NOTE_MISMATCH_GROUP,
    "vcards": ("mismatch_group.vcf",),
}
VCARDS_MISMATCH_PARAMETER: VCARD_TEST_CASE_TYPE = {
    "message": vcard_errors.NOTE_MISMATCH_PARAMETER,
    "vcards": tuple(),
}
VCARDS_MISSING_GROUP: VCARD_TEST_CASE_TYPE = {
    "message": vcard_errors.NOTE_MISSING_GROUP,
    "vcards": ("missing_group.vcf",),
}
VCARDS_MISSING_PARAMETER: VCARD_TEST_CASE_TYPE = {
    "message": vcard_errors.NOTE_MISSING_PARAMETER,
    "vcards": ("missing_photo_param.vcf",),
}
VCARDS_MISSING_PARAM_VALUE: VCARD_TEST_CASE_TYPE = {
    "message": vcard_errors.NOTE_MISSING_PARAM_VALUE,
    "vcards": ("missing_param_value.vcf",),
}
VCARDS_MISSING_PROPERTY: VCARD_TEST_CASE_TYPE = {
    "message": vcard_errors.NOTE_MISSING_PROPERTY,
    "vcards": (
        "missing_properties.vcf",
        "missing_start.vcf",
        "missing_end.vcf",
        "missing_version.vcf",
        "missing_n.vcf",
        "missing_fn.vcf",
    ),
}
VCARDS_MISSING_VALUE_STRING: VCARD_TEST_CASE_TYPE = {
    "message": vcard_errors.NOTE_MISSING_VALUE_STRING,
    "vcards": ("missing_n_value.vcf",),
}

VCARDS_WITH_ERROR: Tuple[VCARD_TEST_CASE_TYPE, ...] = (
    VCARDS_CONTINUATION_AT_START,
    VCARDS_DOT_AT_LINE_START,
    VCARDS_EMPTY_VCARD,
    VCARDS_INVALID_DATE,
    VCARDS_INVALID_LANGUAGE_VALUE,
    VCARDS_INVALID_LINE_SEPARATOR,
    VCARDS_INVALID_PARAM_NAME,
    VCARDS_INVALID_PARAM_VALUE,
    VCARDS_INVALID_PROPERTY_NAME,
    VCARDS_INVALID_SUB_VALUE,
    VCARDS_INVALID_SUB_VALUE_COUNT,
    VCARDS_INVALID_TIME,
    VCARDS_INVALID_TIME_ZONE,
    VCARDS_INVALID_URI,
    VCARDS_INVALID_VALUE,
    VCARDS_INVALID_VALUE_COUNT,
    VCARDS_INVALID_X_NAME,
    VCARDS_MISMATCH_GROUP,
    VCARDS_MISMATCH_PARAMETER,
    VCARDS_MISSING_GROUP,
    VCARDS_MISSING_PARAMETER,
    VCARDS_MISSING_PARAM_VALUE,
    VCARDS_MISSING_PROPERTY,
    VCARDS_MISSING_VALUE_STRING,
)

# Reference cards with errors
VCARDS_REFERENCE_ERRORS = (
    # https://tools.ietf.org/html/rfc2426
    "rfc_2426_a.vcf",
    "rfc_2426_b.vcf",
)


class ValidContentTests(TestCase):
    def test_should_allow_minimal_content(self) -> None:  # pylint: disable=no-self-use
        content = """BEGIN:VCARD\r
VERSION:3.0\r
N:Doe;John;;Mr;\r
FN:John Doe\r
END:VCARD\r
"""
        vcard_validator.VCard(content)

    def test_should_support_all_properties(self) -> None:  # pylint: disable=no-self-use
        content = """BEGIN:VCARD\r
VERSION:3.0\r
PROFILE:VCARD\r
CLASS:PUBLIC\r
SOURCE;VALUE=uri:https://example.org/example.vcf\r
N:Doe;John;Quentin;Mr,Dr;Esq.\r
FN:John Doe\r
NICKNAME:Johnny,Dr Q\r
ORG:Example Organization;Example Department;Example Sub-Department;Example \r
 Office\r
LOGO;VALUE=uri:https://example.org/example.svg\r
TITLE:Assistant assessor\r
URL:https://example.org/example-department/example-sub-department/example-o\r
 ffice\r
EMAIL;TYPE=PREF;TYPE=WORK:jdoe+work-related@example.org\r
TEL;TYPE=PREF;TYPE=MSG;TYPE=WORK:+1234567890\r
TEL;TYPE=FAX;TYPE=WORK:+0123456789\r
ADR:Post office box 1;West wing;1 Some Street;Tinytown;Schwarzwald;12345;Fa\r
 ntasia\r
LABEL:Pb 1\\n1 Some Street\\n12345 Tinytown\\nFantasia\r
PHOTO;VALUE=uri:https://example.org/jdoe.jpg\r
NOTE:Who is this guy anyway? He looks like a double agent from the eighties\r
 \\, talks like a loon\\, and reads comics when he should be working.\r
TZ:+10:30\r
GEO:1.2345;6.7890\r
CATEGORIES:Worker drones,Finance\\, marketing and bull****ing\r
END:VCARD\r
"""
        vcard_validator.VCard(content)

    def test_should_allow_scrambled_case(self) -> None:  # pylint: disable=no-self-use
        content = """bEGIn:VCaRD\r
VErsiON:3.0\r
n:Doe;John;;Mr;\r
fN:John Doe\r
EnD:vcArD\r
"""
        vcard_validator.VCard(content)


class TestVCards(TestCase):
    """Test example vCards"""

    def test_failing(self) -> None:
        """vCards with errors"""
        for test_group in VCARDS_WITH_ERROR:
            for vcard_file in test_group["vcards"]:
                vcard_text = _get_vcard_file(vcard_file)
                assert vcard_text is not None

                with catch_warnings(record=True):
                    self.assertRaisesRegex(
                        vcard_errors.VCardError,
                        escape(test_group["message"]),
                        vcard_validator.VCard,
                        vcard_text,
                        filename=vcard_file,
                    )

    def test_online(self) -> None:
        """vCards in references which are invalid"""
        for vcard_file in VCARDS_REFERENCE_ERRORS:
            vcard_text = _get_vcard_file(vcard_file)
            assert vcard_text is not None

            with catch_warnings(record=True):
                self.assertRaises(vcard_errors.VCardError, vcard_validator.VCard, vcard_text)

    def test_doc(self) -> None:
        """Run DocTests"""
        self.assertEqual(testmod(vcard)[0], 0)
        self.assertEqual(testmod(vcard_definitions)[0], 0)
        self.assertEqual(testmod(vcard_errors)[0], 0)
        self.assertEqual(testmod(vcard_utils)[0], 0)
        self.assertEqual(testmod(vcard_validators)[0], 0)
