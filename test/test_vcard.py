from argparse import Namespace
from unittest import TestCase
from unittest.mock import MagicMock, Mock, patch

from vcard.vcard import main, parse_arguments
from vcard.vcard_errors import UsageError
from vcard.vcard_validator import VcardValidator

ARGUMENTS_WITH_PATH = Namespace(paths=["any"], verbose=False)
ARGUMENTS_WITH_PATHS = Namespace(paths=["any", "another"], verbose=False)


class TestVcard(TestCase):
    @patch("vcard.vcard.parse_arguments")
    @patch("vcard.vcard.VcardValidator", spec=VcardValidator)
    def test_main_succeeds_when_vcard_validator_returns_empty_string(
        self, vcard_validator_mock: MagicMock, parse_arguments_mock: MagicMock
    ) -> None:
        parse_arguments_mock.return_value = ARGUMENTS_WITH_PATH
        vcard_validator_mock.return_value.result = ""
        self.assertEqual(0, main())

    @patch("vcard.vcard.parse_arguments")
    @patch("vcard.vcard.VcardValidator", spec=VcardValidator)
    def test_main_fails_when_vcard_validator_fails(
        self, vcard_validator_mock: MagicMock, parse_arguments_mock: MagicMock
    ) -> None:
        parse_arguments_mock.return_value = ARGUMENTS_WITH_PATH
        vcard_validator_mock.return_value.result = "non-empty"
        self.assertEqual(1, main())

    @patch("vcard.vcard.parse_arguments")
    @patch("vcard.vcard.VcardValidator")
    def test_main_fails_when_vcard_validator_fails_on_first_file(
        self, vcard_validator_mock: MagicMock, parse_arguments_mock: MagicMock
    ) -> None:
        parse_arguments_mock.return_value = ARGUMENTS_WITH_PATHS
        vcard_validator_mock.side_effect = [
            Mock(spec=VcardValidator, result="non-empty"),
            Mock(spec=VcardValidator, result=""),
        ]
        self.assertEqual(1, main())

    @patch("vcard.vcard.parse_arguments")
    def test_main_fails_when_argument_parsing_fails(self, parse_arguments_mock: MagicMock) -> None:
        parse_arguments_mock.side_effect = UsageError("error")
        self.assertEqual(2, main())

    def test_parse_arguments_succeeds_with_single_path(self) -> None:
        path = "/some/path"
        expected_paths = [path]

        actual_paths = parse_arguments([path]).paths

        self.assertEqual(expected_paths, actual_paths)

    def test_parse_arguments_succeeds_with_multiple_paths(self) -> None:
        path1 = "/some/path"
        path2 = "/other/path"
        arguments = [path1, path2]
        expected_paths = arguments

        actual_paths = parse_arguments(arguments).paths

        self.assertEqual(expected_paths, actual_paths)

    def test_parse_arguments_fails_without_path(self) -> None:
        self.assertRaises(SystemExit, parse_arguments, [])

    def test_parse_arguments_fails_with_invalid_argument(self) -> None:
        self.assertRaises(SystemExit, parse_arguments, ["--foo"])

    def test_parse_arguments_verbose_off_by_default(self) -> None:
        path = "/some/path"

        actual_verbosity = parse_arguments([path]).verbose

        self.assertFalse(actual_verbosity)

    def test_parse_arguments_sets_verbose_when_passed(self) -> None:
        path = "/some/path"

        actual_verbosity = parse_arguments(["--verbose", path]).verbose

        self.assertTrue(actual_verbosity)
