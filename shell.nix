let
  pkgs = import ./nixpkgs.nix;
  python = (pkgs.callPackage ./python.nix { }).withPackages (ps: [
    ps.black
    ps.check-manifest
    ps.coverage
    ps.isort
    ps.mypy
    ps.pip
    ps.pylint
    ps.setuptools
    ps.twine
    ps.types-dateutil
    ps.types-setuptools
  ]);
  app = python.pkgs.toPythonApplication (pkgs.callPackage ./derivation.nix { });
  java = pkgs.jetbrains.jdk;
  gradle = pkgs.gradle_7.override {
    inherit java;
  };
  shfmt = pkgs.shfmt;
in
pkgs.mkShell {
  packages = [
    app
    pkgs.bashInteractive
    pkgs.cacert
    pkgs.check-jsonschema
    pkgs.cargo
    pkgs.gitFull
    pkgs.gitlint
    gradle
    python
    pkgs.nix
    pkgs.nixfmt-rfc-style
    pkgs.nodejs
    pkgs.pre-commit
    pkgs.nodePackages.prettier
    pkgs.shellcheck
    shfmt
    pkgs.taplo
    pkgs.yq-go
  ];
  env = {
    LOCALE_ARCHIVE = "${pkgs.glibcLocales}/lib/locale/locale-archive";
    LD_LIBRARY_PATH = pkgs.lib.makeLibraryPath [
      pkgs.e2fsprogs
      pkgs.fontconfig
      pkgs.freetype
      pkgs.xorg.libX11
      pkgs.xorg.libXext
      pkgs.xorg.libXi
      pkgs.xorg.libXrender
      pkgs.xorg.libXtst
    ];
    GRADLE_JAVA_HOME_PARAM = "-Dorg.gradle.java.home=${java}";
  };
  shellHook = ''
    ln --force --no-target-directory --symbolic "${pkgs.lib.getExe python}" python
    ln --force --no-target-directory --symbolic "${pkgs.lib.getExe shfmt}" shfmt
  '';
}
