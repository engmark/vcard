{ pkgs }:
let
  pname = "vcard";
  version = pkgs.lib.removeSuffix "\n" (builtins.readFile ./vcard/version.txt);
  python = pkgs.callPackage ./python.nix { };
in
python.pkgs.buildPythonPackage {
  inherit pname version;

  pyproject = true;

  build-system = [
    python.pkgs.setuptools
  ];

  src = builtins.path {
    path = ./.;
    name = pname;
  };

  propagatedBuildInputs = [
    python.pkgs.python-dateutil
  ];

  checkInputs = [
    python.pkgs.mock
  ];

  nativeCheckInputs = [
    pkgs.unzip
    pkgs.zip
  ];

  pythonImportsCheck = [
    pname
  ];

  postCheck = ''
    zip --test $dist/${pname}-${version}-py2.py3-none-any.whl
  '';

  meta = {
    homepage = "https://gitlab.com/engmark/${pname}";
    downloadPage = "https://pypi.org/project/${pname}/";
    description = "vCard validator, class and utility functions";
    longDescription = builtins.readFile ./README.md;
    mainProgram = "vcard";
    maintainers = [
      pkgs.lib.maintainers.l0b0
    ];
    license = pkgs.lib.licenses.agpl3Plus;
  };
}
